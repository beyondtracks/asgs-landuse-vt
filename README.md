# asgs-landuse-vt

Generate landuse vector tiles from the ASGS Mesh Blocks.

# Mesh Blocks
Keep in mind that ASGS Mesh Blocks are only designed to ["broadly reflect land use"](http://www.abs.gov.au/ausstats/abs@.nsf/Lookup/by%20Subject/1270.0.55.001~July%202016~Main%20Features~Mesh%20Blocks%20%28MB%29~10012).

> The Mesh Block category is not designed to provide a definitive land use map. It is purely indicative of the main land use for a Mesh Block, based on a range of general land use indicators.
> 
> The land use categories and their criteria are described below.
> 
> **Residential**: Generally, residential areas have been separated from other land uses. Residential Mesh Blocks can include houses, duplexes, apartments, serviced/long stay apartments, townhouses, gated communities, complexes, caravan parks, retirement villages, military bases where people live, and prisons.
>
> **Commercial**: Mesh Blocks categorised as commercial will contain a number of businesses, and where possible, will have a zero population count. Some commercial Mesh Blocks may contain population, for example, where a residential flat is above a shop.
>
> **Industrial**: Mesh Blocks categorised as industrial will contain a number of businesses, and where possible, will have a zero population count.
>
> **Parkland**: Mesh Blocks with parkland, nature reserves and other minimal use protected or conserved areas have been categorised as Parkland. Parkland Mesh Blocks may also include any public open space and sporting arena or facility whether enclosed or open to the public, including racecourses, golf courses and stadiums.
>
> **Education**: Education Mesh Blocks aim to capture education facilities and may contain population in non-private dwellings such as boarding schools or universities.
>
> **Hospital/Medical**: Mesh Blocks with hospital or medical facilities have been classified as such. Hospital/Medical Mesh Blocks will also include aged care facilities, which are independent to larger retirement villages.
>
> **Transport**: Mesh Blocks which only contain road or rail features have been categorised as transport.
>
> **Primary Production**: Primary production has replaced the previous category of agricultural. Mesh Blocks categorised as primary production must have more than 50 per cent of their area attributed to a primary production land use, and has been categorised as this using a range of available datasets. Mesh Blocks which were previously categorised as agricultural and did not meet this criteria were categorised as other.
>
> **Water**: Water Mesh Blocks aim to identify water bodies where possible.
Other: Mesh Blocks classified as other are representative of land uses which could not be easily placed in one of the other nine categories due to the nature of the land use, or due to evidence of high mixed use.

## Download Results

https://gitlab.com/beyondtracks/asgs-landuse-vt/-/jobs/artifacts/master/browse?job=build
