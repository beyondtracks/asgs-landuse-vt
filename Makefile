all: MeshBlock_URLs.txt download unzip merge data/ASGS_MB_2016_Landuse.mbtiles

clean:
	rm -rf MeshBlock_URLs.txt zip unzip data

MeshBlock_URLs.txt:
	./src/generateDownloadURLs.sh

download: MeshBlock_URLs.txt
	wget --input-file=$< --trust-server-names --directory-prefix=zip --no-verbose

unzip:
	./src/unzip.sh

merge:
	./src/merge.sh

data/ASGS_MB_2016_Landuse.mbtiles: data/MB_2016.tab
	ogr2ogr \
	    -f GeoJSON \
	    -t_srs 'EPSG:4326' \
	    -lco RFC7946=YES \
	    -sql 'SELECT MB_CATEGORY_NAME_2016 AS landuse FROM MB_2016' \
	    /vsistdout \
	    $< | \
	tippecanoe \
	    -o $@ \
	    --force \
	    --name "ASGS MB 2016 Landuse" \
	    --description "ASGS 2016 Mesh Block derived Landuse" \
	    --attribution "Based on ABS data" \
	    --minimum-zoom=0 \
	    --maximum-zoom=14 \
	    --coalesce-smallest-as-needed \
	    --detect-shared-borders \
	    --reorder --hilbert --coalesce

