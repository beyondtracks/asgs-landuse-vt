#!/bin/bash

mkdir -p data

i=0
for src_file in unzip/*.tab; do
    echo "${src_file}"

    if [ $i -eq 0 ] ; then
        ogr2ogr data/MB_2016.tab "${src_file}"
    else
        ogr2ogr -update -append data/MB_2016.tab "${src_file}" -nln MB_2016
    fi

    i=$((i+1))
done
