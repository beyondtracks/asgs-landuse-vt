#!/bin/sh

curl -o - "http://www.abs.gov.au/AUSSTATS/abs@.nsf/DetailsPage/1270.0.55.001July%202016?OpenDocument" 2> /dev/null | \
    grep --binary-files=text --ignore-case --only-matching '/ausstats[^"]*' | \
    grep -E '(Latest|Previous)$' | \
    grep 'tab.zip' | \
    grep '1270055001_mb' | \
    sed 's/^/http:\/\/www.abs.gov.au/' \
    > MeshBlock_URLs.txt

